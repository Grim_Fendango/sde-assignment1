package com.sde.controllers;

import com.sde.data.PersonRepository;
import com.sde.services.PersonService;
import com.sde.data.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZonedDateTime;
import java.util.Date;

@Controller
public class PersonController {

    private final PersonRepository personRepository;
    private final PersonService personService;

    @Autowired
    public PersonController(PersonRepository personRepository, PersonService personService) {
        this.personRepository = personRepository;
        this.personService = personService;
    }

    @GetMapping("/inputForm")
    public String renderInputForm(Model model) {
        model.addAttribute("person", new Person());

        return "inputForm";
    }

    @PostMapping("/displayDaysAlive")
    public String inputFormSubmit(@ModelAttribute Person person) {
        long daysAlive = personService.calculateDaysAlive(person.getDateOfBirth());
        long martianDaysAlive = personService.calculateMartianDaysAlive(person.getDateOfBirth());

        person.setEntryTimestamp(Instant.now());
        person.setDaysAlive(daysAlive);
        person.setMartianDaysAlive(martianDaysAlive);
        personRepository.save(person);

        return "displayDaysAlive";
    }

    @GetMapping("/displayInputHistory")
    public String renderInputHistory(Model model) {
        model.addAttribute("personList", personRepository.findAll());

        return "displayInputHistory";
    }

}
