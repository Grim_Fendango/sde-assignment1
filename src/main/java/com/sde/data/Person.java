package com.sde.data;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZonedDateTime;

@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "given_name")
    private String givenName;

    @Column(name = "family_name")
    private String familyName;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Column(name = "entry_time_stamp")
    private Instant entryTimestamp;

    @Column(name = "days_alive")
    private long daysAlive;

    @Column(name = "martian_days_alive")
    private long martianDaysAlive;

    public long getId() {
        return id;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Instant getEntryTimestamp() {
        return entryTimestamp;
    }

    public void setEntryTimestamp(Instant entryTimestamp) {
        this.entryTimestamp = entryTimestamp;
    }

    public Long getDaysAlive() {
        return daysAlive;
    }

    public void setDaysAlive(long daysAlive) {
        this.daysAlive = daysAlive;
    }

    public Long getMartianDaysAlive() {
        return martianDaysAlive;
    }

    public void setMartianDaysAlive(long martianDaysAlive) {
        this.martianDaysAlive = martianDaysAlive;
    }
}
