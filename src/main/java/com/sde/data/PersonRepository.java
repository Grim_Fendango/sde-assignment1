package com.sde.data;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by zakariahchitty on 19/09/2016.
 */
public interface PersonRepository extends CrudRepository<Person, Long> {
}
