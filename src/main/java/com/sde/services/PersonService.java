package com.sde.services;

import com.sde.data.Person;
import org.springframework.stereotype.Controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static java.time.temporal.ChronoUnit.DAYS;

@Controller
public class PersonService {

    // Source  : https://en.wikipedia.org/wiki/Timekeeping_on_Mars
    // Section : Time of day
    // Yields a conversion factor of 1.0274912510 days/sol
    static final double EARTH_TO_MARS_SOL_RATE = 1.0274912510;

    public long calculateDaysAlive(LocalDate dateOfBirth) {
        return ChronoUnit.DAYS.between(dateOfBirth, LocalDate.now());
    }

    // Source : https://en.wikipedia.org/wiki/Timekeeping_on_Mars
    public long calculateMartianDaysAlive(LocalDate dateOfBirth) {
        return (long)(calculateDaysAlive(dateOfBirth) * EARTH_TO_MARS_SOL_RATE);
    }
}
