#SDE Assignment 1

##Project Requirements
Java 1.8

##Development Tools
It's recommended that you use IntelliJ as your IDE.

##Setup Guide

###Setting up dev build environment
This project comes with vagrant/ansible configurations aimed to simply environment setup and remove the works on my
machine problem. To setup a new dev environment first make sure that you have vagrant and ansible installed.

For mac users with home brew installed this is simply:

```
brew cask install virtualbox

brew cask install vagrant

brew install ansible

```

For other OSes you can find vagrant download links here https://www.vagrantup.com/downloads.html and ansible
download links here.

Setting up ansible on non mac platforms is on its way soon.

With ansible and vagrant installed do the following:

```
cd vagrant

# Downloads the external roles from ansible galaxy
ansible-galaxy install -p ./roles -r install_roles.yml

# This one should take a while so be patient
vagrant up

# SSHs into the vagrant instance
vagrant ssh
```

And that's it.

The directory of the repository is shared with the vm so you can do most of you work on the host OS (VI sucks).
The repo can be found inside the VM at /home/vagrant/dev or simply ~/dev.

###Useful vagrant commands
To exit the vm while still SSHed simply type exit in the command line.

To shut down the vm simply run:

```
vagrant halt
```

To restart the vm do:

```
vagrant reload
```

To reprovision the vm do:

```
vagrant provision
```

###Setting up with IntelliJ
First of all if using intellij it's recommended that you let gradle generate the initellij project files. Run the following command:

```
vagrant ssh
cd dev
./gradlew idea
```

Then navigate to the project directory (in the host OS) and run assignment1.ipr to open the project in Intellij.

###Running the project

Run the following command:

```
./gradlew devRun
```

NOTE: devRun, automatically checks for db migrations and runs them if required before starting the server!

###Deploying to Heroku

The Heroku tool belt is installed as part of the a ansible provisioning. It is recommended that all deployments to heroku be done from inside the dev vagrant box.

To deploy to Heroku do the following:

```
heroku create
heroku git:remote -a <heroku app name>
heroku addons:create heroku-postgresql
heroku config:set spring.profiles.active=production
git push heroku <git branch to deploy>:master
```

###Running Heroku locally
In some rare cases when you need to test that the heroku procfile is correct and stuff you may want to run heroku locally via run local. 

Do the following:

```
./gradlew stage
heroku local -e .env.local 
```

NOTE: this will run the app using the dev profile. The production profile depends on heroku's out of the box postgresql server which isn't available when doing run local.

###Connecting to Heroku Database
If you need to drop the databasechangelog / databasechangeloglock tables in production or just look at the data do the following:

```
heroku run echo \$JDBC_DATABASE_URL
This prints out the url of the database that the deployed app is currently using

heroku pg:psql
>>> \c <name of database given from first command>
```

###Database Migrations

#####Basics
The project uses liquibase to manage database migrations and schema changes. 

The change log can be found at:

```
src/main/resources/db/changelog/db.changelog.master.yaml
```

The liquibase gradle plugin provides various tasks, however, it is poorly documented. More information about the db migration commands available in gradle be run can be found by running ```./gradlew tasks```. Also check out the [liquibase docs](http://www.liquibase.org/index.html).

The three commands you will mostly use the most are:

* update - Updates the database to the current version, runs all the new changes in the changelog.
* tag - Tags the current database state with <liquibaseCommandValue> for future rollback.
* rollbackCount - Rolls back N number of changesets based on the given -PliquibaseCommandValue

#####My database is corrupted what do I do?
It really depends, firstly liquibase comes in with built in features to rollback changesets. In some cases you need to define a custom rollback script but for some of the built in change types liquibase provised some default rollback implementations. Anyway if the changeset has rollback functionality available you should consider using it. 

To rollback the latest change set do:

```
./gradlew rollbackCount -PliquibaseCommandValue=<number of changesets to rollback>
```

NOTE: you should do the rollback before reverting the commit!

###Ansible

####Adding new roles

The ansible roles are defined in install_roles.yml. Once a new role has been added to this file you will need to
run the following to retrieve it from ansible galaxy:

```
cd vagrant
ansible-galaxy install -p ./roles -r install_roles.yml
```
